using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.InputSystem;

namespace InventoryDemo.Input
{
    public abstract class InputEventWrapper
    {
        protected InputAction wrappedAction;

        protected List<InputCallbackWrapper> registeredCallbacks = new List<InputCallbackWrapper>();
    }

    public class StartedWrapper : InputEventWrapper
    {
        public StartedWrapper(InputAction action)
        {
            wrappedAction = action;
        }

        public static StartedWrapper operator +(StartedWrapper startedWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            if (startedWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback) != null)
            {
                Debug.LogError("Adding an existing callback!: " + callback.Method.Name);
            }
            else
            {
                //Debug.Log("Adding a callback: " + callback.Method.Name);
            }

            InputCallbackWrapper wrappedCallback = new InputCallbackWrapper(callback);
            startedWrapper.registeredCallbacks.Add(wrappedCallback);
            startedWrapper.wrappedAction.started += wrappedCallback;
            return startedWrapper;
        }

        public static StartedWrapper operator -(StartedWrapper startedWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            InputCallbackWrapper callbackToRemove = startedWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback);

            if (callbackToRemove != null)
            {
                startedWrapper.wrappedAction.started -= callbackToRemove;
                startedWrapper.registeredCallbacks.Remove(callbackToRemove);
                //Debug.Log("Removing callback: " + callback.Method.Name);
            }
            else
            {
                Debug.LogWarning("Trying to remove nonexisting callback: " + callback.Method.Name);
            }

            return startedWrapper;
        }
    }

    public class PerformedWrapper : InputEventWrapper
    {
        public PerformedWrapper(InputAction action)
        {
            wrappedAction = action;
        }

        public static PerformedWrapper operator +(PerformedWrapper performedWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            if (performedWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback) != null)
            {
                Debug.LogError("Adding an existing callback!: " + callback.Method.Name);
            }
            else
            {
                //Debug.Log("Adding a callback: " + callback.Method.Name);
            }

            InputCallbackWrapper wrappedCallback = new InputCallbackWrapper(callback);
            performedWrapper.registeredCallbacks.Add(wrappedCallback);
            performedWrapper.wrappedAction.performed += wrappedCallback;
            return performedWrapper;
        }

        public static PerformedWrapper operator -(PerformedWrapper performedWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            InputCallbackWrapper callbackToRemove = performedWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback);

            if (callbackToRemove != null)
            {
                performedWrapper.wrappedAction.performed -= callbackToRemove;
                performedWrapper.registeredCallbacks.Remove(callbackToRemove);
                // Debug.Log("Removing callback: " + callback.Method.Name);
            }
            else
            {
                Debug.LogWarning("Trying to remove nonexisting callback: " + callback.Method.Name);
            }

            return performedWrapper;
        }
    }

    public class CanceledWrapper : InputEventWrapper
    {
        public CanceledWrapper(InputAction action)
        {
            wrappedAction = action;
        }

        public static CanceledWrapper operator +(CanceledWrapper canceledWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            if (canceledWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback) != null)
            {
                Debug.LogError("Adding an existing callback!: " + callback.Method.Name);
            }
            else
            {
                // Debug.Log("Adding a callback: " + callback.Method.Name);
            }

            InputCallbackWrapper wrappedCallback = new InputCallbackWrapper(callback);
            canceledWrapper.registeredCallbacks.Add(wrappedCallback);
            canceledWrapper.wrappedAction.canceled += wrappedCallback;
            return canceledWrapper;
        }

        public static CanceledWrapper operator -(CanceledWrapper canceledWrapper, System.Action<InputAction.CallbackContext> callback)
        {
            InputCallbackWrapper callbackToRemove = canceledWrapper.registeredCallbacks.Find(callbackWrapper => callbackWrapper.WrappedCallbackAction == callback);

            if (callbackToRemove != null)
            {
                canceledWrapper.wrappedAction.canceled -= callbackToRemove;
                canceledWrapper.registeredCallbacks.Remove(callbackToRemove);
                // Debug.Log("Removing callback: " + callback.Method.Name);
            }
            else
            {
                Debug.LogWarning("Trying to remove nonexisting callback: " + callback.Method.Name);
            }

            return canceledWrapper;
        }
    }
}
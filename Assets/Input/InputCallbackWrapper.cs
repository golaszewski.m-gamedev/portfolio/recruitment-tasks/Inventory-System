using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.InputSystem;

namespace InventoryDemo.Input
{
    public class InputCallbackWrapper
    {
        public System.Action<InputAction.CallbackContext> WrappedCallbackAction { get; private set; }

        private bool callbackActive = false;

        public InputCallbackWrapper(System.Action<InputAction.CallbackContext> callbackAction)
        {
            WrappedCallbackAction = callbackAction;

            InputManager.Instance.StartCoroutine(QueueCallbackAction());
        }

        private IEnumerator QueueCallbackAction()
        {
            yield return new WaitForEndOfFrame();

            callbackActive = true;
        }

        public void PerformCallbackAction(InputAction.CallbackContext context)
        {
            if (callbackActive)
            {
                WrappedCallbackAction.Invoke(context);
            }
        }

        public static implicit operator System.Action<InputAction.CallbackContext>(InputCallbackWrapper callbackWrapper)
        {
            return callbackWrapper.PerformCallbackAction;
        }
    }
}
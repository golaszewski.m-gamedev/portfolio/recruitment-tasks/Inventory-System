using System.Linq;
using System.Collections;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace InventoryDemo.Input
{   
    public class InputManager : MonoBehaviour
    {
        [SerializeField]
        private PlayerInput playerInput;
        public PlayerInput PlayerInput { get => playerInput; set => playerInput = value; }

        public static InputManager Instance { get; private set; }

        // makes changing actionmap names easier
        public enum AvailableActionMaps
        {
            Gameplay,
            Menu
        }

        public class MenuInputActions
        {
            // UI Controls
            public InputActionWrapper Navigate { get; set; }
            public InputActionWrapper Submit { get; set; }
            public InputActionWrapper Cancel { get; set; }
            public InputActionWrapper ToggleInventory { get; set; }

            public MenuInputActions(PlayerInput PlayerInput)
            {
                InputActionMap menuActionMap = PlayerInput.actions.FindActionMap(AvailableActionMaps.Menu.ToString());

                Navigate = new InputActionWrapper(menuActionMap.FindAction("Navigate"));
                Submit = new InputActionWrapper(menuActionMap.FindAction("Submit"));
                Cancel = new InputActionWrapper(menuActionMap.FindAction("Cancel"));
                ToggleInventory = new InputActionWrapper(menuActionMap.FindAction("ToggleInventory"));
            }
        }

        public class GameplayInputActions
        {
            public InputActionWrapper Movement { get; set; }
            public InputActionWrapper Interaction { get; set; }
            public InputActionWrapper LookAround { get; set; }
            public InputActionWrapper ToggleInventory { get; set; }

            public GameplayInputActions(PlayerInput PlayerInput)
            {
                InputActionMap gameplayActionMap = PlayerInput.actions.FindActionMap(AvailableActionMaps.Gameplay.ToString());

                Movement = new InputActionWrapper(gameplayActionMap.FindAction("Movement"));
                Interaction = new InputActionWrapper(gameplayActionMap.FindAction("Interaction"));
                LookAround = new InputActionWrapper(gameplayActionMap.FindAction("LookAround"));
                ToggleInventory = new InputActionWrapper(gameplayActionMap.FindAction("ToggleInventory"));
            }
        }

        public MenuInputActions MenuActions { get; set; }
        public GameplayInputActions GameplayActions { get; set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                throw new System.Exception("Error! Attempted to instantiate second " + GetType().ToString() + " singleton!");
            }
            else
            {
                Instance = this;
            }

            MenuActions = new MenuInputActions(PlayerInput);
            GameplayActions = new GameplayInputActions(PlayerInput);

            /*
            foreach (InputDevice inputDevice in InputSystem.devices)
            {
                InputSystem.QueueConfigChangeEvent(inputDevice);
            }
            */

            InputSystem.onDeviceChange +=
            (device, change) =>
            {
                if (change.Equals(InputDeviceChange.Removed))
                {
                    //Checking if device belonged to player
                    foreach (InputDevice pairedDevice in PlayerInput.user.pairedDevices)
                    {
                        if (pairedDevice == device)
                        {
                            PlayerInput.user.UnpairDevice(device);
                            StartCoroutine(HandleDisconnection(PlayerInput));
                        }
                    }
                }

                if (change.Equals(InputDeviceChange.Added))
                {
                    if (PlayerInput.hasMissingRequiredDevices)
                    {
                        // Can be used to attempt to reconnect a device
                        // InputUser.PerformPairingWithDevice(device, PlayerInput.user);
                        // GameManager.Instance.PlayerControllerRegained();
                    }
                }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
                // Debug device status logging
                switch (change)
                {
                    case InputDeviceChange.Added:
                        Debug.Log("New device added: " + device);
                        break;

                    case InputDeviceChange.Removed:
                        Debug.Log("Device removed: " + device);
                        break;

                    case InputDeviceChange.Reconnected:
                        Debug.Log("Device reconnected: " + device);
                        break;

                    case InputDeviceChange.Disconnected:
                        Debug.Log("Device disconnected: " + device);
                        break;

                    case InputDeviceChange.Enabled:
                        Debug.Log("Device enabled: " + device);
                        break;

                    case InputDeviceChange.Disabled:
                        Debug.Log("Device disabled: " + device);
                        break;

                    case InputDeviceChange.ConfigurationChanged:
                        Debug.Log("Device configuration changed: " + device);
                        break;

                    case InputDeviceChange.UsageChanged:
                        Debug.Log("Device usage changed: " + device);
                        break;
                }

                // LogDetailedPlayersAndDevicesStatus();
#endif
            };

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            InputUser.onChange +=
            (user, change, device) =>
            {
                switch (change)
                {
                    case InputUserChange.Added:
                        Debug.Log("New input user added: " + user.id);
                        break;
                    case InputUserChange.Removed:
                        Debug.Log("Input user removed: " + user.id);
                        break;
                    case InputUserChange.DevicePaired:
                        Debug.Log("New device paired to user: " + user.id + ", " + device.displayName);
                        break;
                    case InputUserChange.DeviceUnpaired:
                        Debug.Log("Device unpaired from user: " + user.id + ", " + device.displayName);
                        break;
                    case InputUserChange.DeviceLost:
                        Debug.Log("Device lost for user: " + user.id + ", " + device.displayName);
                        break;
                    case InputUserChange.DeviceRegained:
                        Debug.Log("Device regained for user: " + user.id + ", " + device.displayName);
                        break;
                    case InputUserChange.AccountChanged:
                        Debug.Log("Account changed for user: " + user.id);
                        break;
                    case InputUserChange.AccountNameChanged:
                        Debug.Log("Account name changed for user: " + user.id);
                        break;
                    case InputUserChange.AccountSelectionInProgress:
                        Debug.Log("Account selection in progress for user: " + user.id);
                        break;
                    case InputUserChange.AccountSelectionCanceled:
                        Debug.Log("Account selection cancelled for user: " + user.id);
                        break;
                    case InputUserChange.AccountSelectionComplete:
                        Debug.Log("Account selection completed for user: " + user.id);
                        break;
                    case InputUserChange.ControlSchemeChanged:
                        Debug.Log("Control scheme changed for user: " + user.id);
                        break;
                    case InputUserChange.ControlsChanged:
                        Debug.Log("Controls changed for user: " + user.id);
                        break;
                }

                // LogDetailedPlayersAndDevicesStatus();
            };
#endif
        }

        private IEnumerator HandleDisconnection(PlayerInput playerInput)
        {
            // This can be used to await re-connection of a particular device, or wholly replaced with any other method call
            // GameManager.Instance.PlayerControllerLost();
            yield break;
        }

        private void Update()
        {
            CheckForPlayerOneControllerAutoSwap();
        }

        /// <summary>
        /// Manual management of hot-swapping of devices
        /// </summary>
        private void CheckForPlayerOneControllerAutoSwap()
        {
            foreach (InputDevice inputDevice in PlayerInput.devices)
            {
                // Check for press of any action
                foreach (InputControl inputControl in inputDevice.allControls)
                {
                    // If something is pressed, player 1 is currently using their device and there's no need to swap
                    if (!IsNoisyMouseInput(inputDevice, inputControl) && !inputControl.noisy && inputControl.IsPressed())
                    {
                        return;
                    }
                }
            }

            // If player device was not used this frame
            // check across free devices for a hot-swap
            bool devicePairingRequested = false;
            KeyValuePair<Keyboard, Mouse> keyboardAndMouse = new KeyValuePair<Keyboard, Mouse>();
            foreach (InputDevice inputDevice in InputUser.GetUnpairedInputDevices())
            {
                //Debug.Log("Unpaired device: " + inputDevice.displayName);

                if (inputDevice is Keyboard || inputDevice is Mouse)
                {
                    if (inputDevice is Keyboard && keyboardAndMouse.Key == null)
                    {
                        // Assign keyboard to the pair
                        keyboardAndMouse = new KeyValuePair<Keyboard, Mouse>(inputDevice as Keyboard, keyboardAndMouse.Value);
                    }
                    else if (inputDevice is Mouse && keyboardAndMouse.Value == null)
                    {
                        // Assign mouse to the pair
                        keyboardAndMouse = new KeyValuePair<Keyboard, Mouse>(keyboardAndMouse.Key, inputDevice as Mouse);
                    }

                    // Check for press of any action
                    foreach (InputControl inputControl in inputDevice.allControls)
                    {
                        // If something is pressed and the device is valid
                        if (!IsNoisyMouseInput(inputDevice, inputControl) && !inputControl.noisy && inputControl.IsPressed())
                        {
                            //Debug.Log(inputDevice.displayName + " requests pairing. Performed action: " + inputControl.displayName + " looking for another device of Keyboard & Mouse set");

                            devicePairingRequested = true;
                        }
                    }

                    // Attempt to pair to mouse&keyboard control scheme only if a full set of controls has been found
                    if (devicePairingRequested && keyboardAndMouse.Key != null && keyboardAndMouse.Value != null)
                    {
                        //Debug.Log(keyboardAndMouse.Key.displayName + " " + keyboardAndMouse.Value.displayName + " set found, assigning to player");

                        //Unpairing old devices upon first devide pairing, then adding the rest the rest without doing so
                        InputUser.PerformPairingWithDevice(keyboardAndMouse.Key, PlayerInput.user, InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                        InputUser.PerformPairingWithDevice(keyboardAndMouse.Value, PlayerInput.user, InputUserPairingOptions.None);
                        
                        PlayerInput.user.ActivateControlScheme("KeyboardAndMouse");
                        return;
                    }
                }
                else
                {
                    InputControlScheme? deviceControllScheme = InputControlScheme.FindControlSchemeForDevice(inputDevice, PlayerInput.actions.controlSchemes);

                    if (deviceControllScheme == null)
                    {
                        // Device is not supported in-game
                        continue;
                    }

                    // Check for press of any action
                    foreach (InputControl inputControl in inputDevice.allControls)
                    {
                        // If something is pressed and the device is valid
                        if (!inputControl.noisy && inputControl.IsPressed())
                        {
                            //Debug.Log(inputDevice.displayName + " is being paired");
                            // Discovered a free alternative device, that is apparently being used. Switch player one to that device!
                            InputUser.PerformPairingWithDevice(inputDevice, PlayerInput.user, InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                            PlayerInput.user.ActivateControlScheme(deviceControllScheme.Value);
                            return;
                        }
                    }
                }
            }
        }

        // Those mouse inputs generate passive inputs, and yet for some reason are not marked as noisy in the input system.
        // Filtering out manually
        private string[] noisyMouseInputs = { "position", "clickcount", "x", "y" };

        private bool IsNoisyMouseInput(InputDevice inputDevice, InputControl inputControl) 
        {
            return inputDevice is Mouse && noisyMouseInputs.Contains(inputControl.name.ToLower());
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            ReEnablePlayer();
        }

        //Fix for playerInputs disabling itself when switching unity scenes
        public void ReEnablePlayer()
        {
            foreach (InputAction inputAction in PlayerInput.currentActionMap.actions)
            {
                inputAction.Enable();
            }
        }

        public void SwitchPlayerActionMap(AvailableActionMaps newActionMap)
        {
            PlayerInput.SwitchCurrentActionMap(newActionMap.ToString());
        }


#if UNITY_EDITOR || DEVELOPMENT_BUILD
        /// <summary>
        /// Useful when debugging the exact issue with multuplayer projects, but takes up a lot of log
        /// </summary>
        public void LogDetailedPlayersAndDevicesStatus()
        {
            string debugMessage = "INPUT DEBUG DUMP:\n";

            debugMessage += "Player Input: " + PlayerInput + "\n";
            debugMessage += "Unput Users:\n";
            foreach (InputUser user in InputUser.all)
            {
                debugMessage += "user " + user.id + "\n";
            }
            debugMessage += "Current input user: " + PlayerInput.user.id + "\n";
            debugMessage += "Unput user valid?: " + PlayerInput.user.valid + "\n";

            if (PlayerInput.currentActionMap != null)
            {
                debugMessage += "Action map: " + PlayerInput.currentActionMap.name + "\n";
            } 

            debugMessage += "Control scheme: " + PlayerInput.currentControlScheme + "\n";

            if (PlayerInput.user.valid)
            {
                debugMessage += "Control scheme (user): " + PlayerInput.user.controlScheme.ToString() + "\n";
            }
            
            debugMessage += "Devices: ";

            foreach (InputDevice device in PlayerInput.devices)
            {
                debugMessage += device.name + ", ";
            }
            debugMessage += "\n\n";
            

            debugMessage += "No. of devices: " + InputSystem.devices.Count + "\n";

            debugMessage += "Devices: ";
            foreach (InputDevice device in InputSystem.devices)
            {
                debugMessage += device.name + ", ";
            }

            debugMessage += "\n\n";
            Debug.Log(debugMessage);
        }
#endif
    }
}

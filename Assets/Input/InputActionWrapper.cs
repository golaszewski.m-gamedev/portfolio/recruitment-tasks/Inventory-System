using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.InputSystem;

namespace InventoryDemo.Input
{
    public class InputActionWrapper
    {
        public InputAction WrappedAction { get; private set; }

        public StartedWrapper started;
        public PerformedWrapper performed;
        public CanceledWrapper canceled;

        public InputActionWrapper(InputAction action)
        {
            WrappedAction = action;

            started = new StartedWrapper(WrappedAction);
            performed = new PerformedWrapper(WrappedAction);
            canceled = new CanceledWrapper(WrappedAction);
        }
    }
}
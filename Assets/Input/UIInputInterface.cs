using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.EventSystems;

namespace InventoryDemo.Input
{
    [RequireComponent(typeof(InputSystemUIInputModule))]
    public class UIInputInterface : MonoBehaviour
    {
        [SerializeField]
        private InputActionReference submit;
        [SerializeField]
        private InputActionReference cancel;
        [SerializeField]
        private InputActionReference navigation;

        private InputSystemUIInputModule inputSystemUIInputModule;

        private Coroutine repeatedNavigationCoroutine;
        private MoveDirection currentNaviagtionDirection;

        private void Awake()
        {
            inputSystemUIInputModule = GetComponent<InputSystemUIInputModule>();
        }

        private void OnEnable()
        {
            submit.action.performed += SubmitCallback;
            cancel.action.performed += CancelCallback;
            navigation.action.started += StartNavigationCallback;
            navigation.action.performed += UpdateNavigationDirection;
            navigation.action.canceled += StopNavigationCallback;
        }
        
        private void OnDisable()
        {
            submit.action.performed -= SubmitCallback;
            cancel.action.performed -= CancelCallback;
            navigation.action.started -= StartNavigationCallback;
            navigation.action.performed -= UpdateNavigationDirection;
            navigation.action.canceled -= StopNavigationCallback;
        }

        private void SubmitCallback(InputAction.CallbackContext context)
        {
            ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
        }

        private void CancelCallback(InputAction.CallbackContext context)
        {
            ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, new BaseEventData(EventSystem.current), ExecuteEvents.cancelHandler);
        }

        private MoveDirection GenerateDirectionFromVector(Vector2 navigationDirection)
        {
            if (Mathf.Abs(navigationDirection.y) >= Mathf.Abs(navigationDirection.x))
            {
                // safe due to deadzones
                if (navigationDirection.y > 0)
                {
                    return MoveDirection.Up;
                }
                else
                {
                    return MoveDirection.Down;
                }
            }
            else
            {
                // safe due to deadzones
                if (navigationDirection.x > 0)
                {
                    return MoveDirection.Right;
                }
                else
                {
                    return MoveDirection.Left;
                }
            }
        }

        private void StartNavigationCallback(InputAction.CallbackContext context)
        {
            AxisEventData axisEventData = new AxisEventData(EventSystem.current);

            axisEventData.moveDir = currentNaviagtionDirection = GenerateDirectionFromVector(context.ReadValue<Vector2>());

            ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);

            repeatedNavigationCoroutine = StartCoroutine(RepeatedNavigation(currentNaviagtionDirection));
        }

        private void UpdateNavigationDirection(InputAction.CallbackContext context)
        {
            MoveDirection newDirection = GenerateDirectionFromVector(context.ReadValue<Vector2>());

            if (newDirection != currentNaviagtionDirection)
            {
                if (repeatedNavigationCoroutine != null) 
                {
                    StopCoroutine(repeatedNavigationCoroutine);
                }

                currentNaviagtionDirection = newDirection;
                repeatedNavigationCoroutine = StartCoroutine(RepeatedNavigation(currentNaviagtionDirection));
            }
        }

        private void StopNavigationCallback(InputAction.CallbackContext context)
        {
            if (repeatedNavigationCoroutine != null)
            {
                StopCoroutine(repeatedNavigationCoroutine);
            }
        }

        private IEnumerator RepeatedNavigation(MoveDirection direction)
        {
            //initialDelay
            yield return new WaitForSeconds(inputSystemUIInputModule.moveRepeatDelay);

            while (true)
            {
                AxisEventData axisEventData = new AxisEventData(EventSystem.current);
                axisEventData.moveDir = direction;
                ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, axisEventData, ExecuteEvents.moveHandler);

                yield return new WaitForSeconds(inputSystemUIInputModule.moveRepeatRate);
            }
        }
    }
}
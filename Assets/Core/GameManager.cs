using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        [SerializeField]
        private Transform player;
        public Transform Player { get => player; set => player = value; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                throw new System.Exception("Error! Attempted to instantiate second " + GetType().ToString() + " singleton!");
            }
            else
            {
                Instance = this;
            }

            DontDestroyOnLoad(gameObject);
        }
    }
}
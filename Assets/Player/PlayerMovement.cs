using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using InventoryDemo.Input;

namespace InventoryDemo
{
    /// <summary>
    /// Based on Unity 2021 example project
    /// </summary>
    public class PlayerMovement : MonoBehaviour
    {
        public CharacterController controller;

        [SerializeField]
        private float speed = 12f;

        [SerializeField]
        private float gravity = -10f;
        [SerializeField]
        private Transform groundCheck;
        [SerializeField]
        private float groundDistance = 0.4f;
        [SerializeField]
        private LayerMask groundMask;

        private Vector3 velocity;
        private bool isGrounded;

        private Coroutine movementCoroutine;

        private InputActionWrapper movementAction;

        private void Awake()
        {
            movementAction = InputManager.Instance.GameplayActions.Movement;
        }

        private void OnEnable()
        {
            movementAction.started += StartMovement;
            movementAction.canceled += StopMovement;
        }

        private void OnDisable()
        {
            movementAction.started -= StartMovement;
            movementAction.canceled -= StopMovement;
        }

        private void StartMovement(InputAction.CallbackContext context)
        {
            movementCoroutine = StartCoroutine(MovementCoroutine());
        }

        private void StopMovement(InputAction.CallbackContext context)
        {
            if (movementCoroutine != null)
            {
                StopCoroutine(movementCoroutine);
            }
        }

        private IEnumerator MovementCoroutine()
        {
            while (true)
            {
                var direction = movementAction.WrappedAction.ReadValue<Vector2>();
                Vector3 move = transform.right * direction.x + transform.forward * direction.y;

                controller.Move(move * speed * Time.deltaTime);

                yield return new WaitForEndOfFrame();
            }
        }

        private void SimulateGravity()
        {
            // stick to surface
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }

        private void Update()
        {
            SimulateGravity();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using InventoryDemo.Input;

namespace InventoryDemo
{
    /// <summary>
    /// Based on Unity 2021 example project
    /// </summary>
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField]
        private float mouseSensitivity = 100f;
        [SerializeField]
        private Transform playerBody;

        private float xRotation = 0f;

        private InputActionWrapper lookAroundAction;

        private void Awake()
        {
            lookAroundAction = InputManager.Instance.GameplayActions.LookAround;
        }

        private void ProcessMouseInput()
        {
            if (!lookAroundAction.WrappedAction.enabled) 
            {
                return;
            }

            Vector2 delta = lookAroundAction.WrappedAction.ReadValue<Vector2>();
            float mouseX = delta.x;
            float mouseY = delta.y;

            mouseX *= mouseSensitivity * Time.deltaTime;
            mouseY *= mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            playerBody.Rotate(Vector3.up * mouseX);
        }

        // Update is called once per frame
        private void Update()
        {
            ProcessMouseInput();
        }
    }
}
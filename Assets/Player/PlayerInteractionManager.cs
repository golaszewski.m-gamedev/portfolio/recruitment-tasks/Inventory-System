using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using InventoryDemo.Input;
using InventoryDemo.UI;

namespace InventoryDemo
{
    public class PlayerInteractionManager : MonoBehaviour
    {
        [SerializeField]
        private float interactionRange;

        private IInteractable currentlytargettedInteractable = null;

        private void OnEnable()
        {
            InputManager.Instance.GameplayActions.Interaction.performed += PerformInteraction;
        }

        private void OnDisable()
        {
            InputManager.Instance.GameplayActions.Interaction.performed -= PerformInteraction;
        }

        private void PerformInteraction(InputAction.CallbackContext context)
        {
            if (currentlytargettedInteractable != null)
            {
                currentlytargettedInteractable.Interact();
            }
        }

        private void CheckForInteractables()
        {
            RaycastHit hit;
            Vector3 lookDir = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, lookDir, out hit, interactionRange))
            {
                if (hit.collider != null)
                {
                    IInteractable hitInteractable = hit.collider.GetComponent<IInteractable>();
                    // select new interactable
                    if (hitInteractable != null)
                    {
                        if (currentlytargettedInteractable != null)
                        {
                            // same selection as before
                            if (hitInteractable == currentlytargettedInteractable)
                            {
                                return;
                            }
                            // hotswap scenario
                            else
                            {
                                currentlytargettedInteractable.Deselect();
                            }
                        }

                        currentlytargettedInteractable = hitInteractable;
                        currentlytargettedInteractable.Select();

                        UserInterfaceController.Instance.DisplayInteractionText(currentlytargettedInteractable.GetInteractionName());
                    }
                    // look away from existing interactable
                    else if (currentlytargettedInteractable != null) 
                    {
                        currentlytargettedInteractable.Deselect();
                        currentlytargettedInteractable = null;

                        UserInterfaceController.Instance.HideInteractionText();
                    }
                }
            }
        }

        private void Update()
        {
            CheckForInteractables();
        }
    }
}
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    // assuming singleplayer game
    public static class InventoryManager
    {
        // left hardcoded due to taks specyfics and UI display restrictions
        // in a larger-scale project might be generated procedurally at runtime (especially if large)
        private static List<InventorySlot> inventory = new List<InventorySlot>
        {
            new InventorySlot(),
            new InventorySlot(),
            new InventorySlot(),
            new InventorySlot(),
            new InventorySlot(),
            new InventorySlot()
        };
        public static List<InventorySlot> Inventory { get => inventory; private set => inventory = value; }

        // number assumed at random
        private static readonly uint maxStackSize = 64;

        public static event System.Action InventoryModified;

        private static InventorySlot FindSlotForItem(ItemData itemType)
        {
            return inventory.FirstOrDefault(
                slot => slot.StoredItem == null
                || (itemType.Stackable && slot.StoredItem.ItemId == itemType.ItemId && slot.ItemAmount < maxStackSize));
        }

        public static bool CanAddItem(ItemData itemType)
        {
            return FindSlotForItem(itemType) != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addedItem">Item to add</param>
        /// <returns>Information wether the addition was performed succesfully</returns>
        public static bool TryAddItem(ItemData addedItem) 
        {
            InventorySlot availableSlot = FindSlotForItem(addedItem);

            if (availableSlot != null)
            {
                AddItem(addedItem, availableSlot);
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void AddItem(ItemData addedItem, InventorySlot modifiedSlot) 
        {
            // adding a clone of the reference object
            modifiedSlot.StoredItem = Object.Instantiate(addedItem);
            ++modifiedSlot.ItemAmount;

            InventoryModified?.Invoke();
        }

        public static void DeleteItem(InventorySlot modifiedSlot) 
        {
            if (modifiedSlot.ItemAmount > 1)
            {
                --modifiedSlot.ItemAmount;
            }
            else if (modifiedSlot.ItemAmount == 1) 
            {
                modifiedSlot.ItemAmount = 0;
                modifiedSlot.StoredItem = null;

                // Moving emptied slot to the back of the Inventory list
                Inventory.Remove(modifiedSlot);
                Inventory.Add(modifiedSlot);
            }
            
            InventoryModified?.Invoke();
        }

        public static void DropItem(InventorySlot modifiedSlot)
        {
            // This can cause the item to spawn in an inaccessible space
            // should be double-checked by a raycast and spawned in Min(raycastHitPosition / maxDropRange)
            Object.Instantiate(modifiedSlot.StoredItem.ItemRepresentation,
                GameManager.Instance.Player.position + GameManager.Instance.Player.forward,
                new Quaternion());
            DeleteItem(modifiedSlot);
        }

        public static void RefreshInventory()
        {
            InventoryModified?.Invoke();
        }
    }
}
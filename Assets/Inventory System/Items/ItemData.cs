using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace InventoryDemo
{
    [CreateAssetMenu(fileName ="New Item Data", menuName ="InventoryDemo/Items/Generic")]
    public abstract class ItemData : ScriptableObject
    {
        [SerializeField, ScriptableObjectId]
        protected string itemId;
        public string ItemId { get => itemId; protected set => itemId = value; }

        [Header("Core info")]
        [SerializeField]
        protected string itemDisplayName;
        public string ItemDisplayName { get => itemDisplayName; protected set => itemDisplayName = value; }

        [SerializeField, TextArea(1,100)]
        protected string itemDescription;
        public string ItemDescription { get => itemDescription; protected set => itemDescription = value; }

        [SerializeField]
        protected Sprite itemSprite;
        public Sprite ItemSprite { get => itemSprite; protected set => itemSprite = value; }


        [SerializeField]
        protected ItemWorldRepresentation itemRepresentation;
        public ItemWorldRepresentation ItemRepresentation { get => itemRepresentation; protected set => itemRepresentation = value; }

        private bool usable = true;
        /// <summary>
        /// Currently auto-set to true but exists to easily allow adding support for fluff-only items in the future
        /// </summary>
        public virtual bool Usable { get => usable; protected set => usable = value; }

        private string usageName = "Use";
        /// <summary>
        /// Can be overriden to change usage name of certain item categories
        /// </summary>
        public virtual string UsageName { get => usageName; protected set => usageName = value; }

        private bool consumable = false;
        /// <summary>
        /// Should an item instance be deleted upon usage
        /// </summary>
        public virtual bool Consumable { get => consumable; protected set => consumable = value; }

        private bool stackable = true;
        /// <summary>
        /// Can multiple items fit in the same slot
        /// </summary>
        public virtual bool Stackable { get => stackable; protected set => stackable = value; }

        public virtual void UseItem() 
        {
            throw new System.NotImplementedException();
        }
    }
}
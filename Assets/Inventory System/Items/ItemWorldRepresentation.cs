using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public class ItemWorldRepresentation : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private ItemData representedItem;
        public ItemData RepresentedItem { get => representedItem; set => representedItem = value; }

        void IInteractable.Select()
        {
            // draft:
            // outline shader on
        }

        void IInteractable.Deselect()
        {
            // draft:
            // outline shader off
        }

        void IInteractable.Interact()
        {
            if (InventoryManager.TryAddItem(representedItem)) 
            {
                Destroy(gameObject);
            }
        }

        string IInteractable.GetInteractionName()
        {
            string interactionDescription = string.Format("<b>{0}</b>", representedItem.ItemDisplayName);

            if (InventoryManager.CanAddItem(representedItem))
            {
                interactionDescription += "\nTake";
            }
            else
            {
                interactionDescription += "\nInventory full";
            }

            return interactionDescription;
        }
    }
}
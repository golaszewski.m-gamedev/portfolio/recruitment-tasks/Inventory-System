using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public class EquippableItemData : ItemData
    {
        public override bool Stackable { get => false; }
        public override string UsageName { get => "Equip"; }

        public bool IsEquipped { get; protected set; } = false;
        
        public override void UseItem()
        {
            // In a proper game this should be checked for player having available slot beforehand
            IsEquipped = !IsEquipped;
            InventoryManager.RefreshInventory();

            Debug.Log(string.Format("{0} {1}", IsEquipped ? "Equipped" : "Unequipped", ItemDisplayName));

            // Now Apply / Remove all item effects in a derived class based on the current IsEquipped value
        }
    }
}
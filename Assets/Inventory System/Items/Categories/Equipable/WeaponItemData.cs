using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    [CreateAssetMenu(fileName = "New Weapon Data", menuName = "InventoryDemo/Items/Equippables/Weapon")]
    public class WeaponItemData : EquippableItemData
    {
        [Header("Weapon parameters")]
        [SerializeField]
        private uint damageValue;
        public uint DamageValue { get => damageValue; protected set => damageValue = value; }

        public override void UseItem()
        {
            base.UseItem();

            Debug.Log(string.Format("Damage {0} by {1}", IsEquipped ? "increased" : "decreased", DamageValue));
        }
    }
}
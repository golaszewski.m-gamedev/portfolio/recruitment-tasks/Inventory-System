using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    [CreateAssetMenu(fileName = "New Armor Piece Data", menuName = "InventoryDemo/Items/Equippables/Armor")]
    public class ArmorItemData : EquippableItemData
    {
        [Header("Armor parameters")]
        [SerializeField]
        private uint armorValue;
        public uint ArmorValue { get => armorValue; protected set => armorValue = value; }

        public override void UseItem()
        {
            base.UseItem();

            Debug.Log(string.Format("Armor {0} by {1}", IsEquipped ? "increased" : "decreased", ArmorValue ));
        }
    }
}
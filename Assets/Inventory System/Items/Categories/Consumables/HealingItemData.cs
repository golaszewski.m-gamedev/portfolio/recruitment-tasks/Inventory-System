using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    [CreateAssetMenu(fileName = "New Healing Item Data", menuName = "InventoryDemo/Items/Consumable/Healing Item")]
    public class HealingItemData : ConsumableItemData
    {
        [Header("Healing effects")]
        [SerializeField]
        private float healingAmount;
        public float HealingAmount { get => healingAmount; protected set => healingAmount = value; }

        public override void UseItem()
        {
            Debug.Log("You are healed for " + HealingAmount);
        }
    }
}
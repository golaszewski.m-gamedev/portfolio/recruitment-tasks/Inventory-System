using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public class ConsumableItemData : ItemData
    {
        public override bool Consumable { get => true; }
    }
}
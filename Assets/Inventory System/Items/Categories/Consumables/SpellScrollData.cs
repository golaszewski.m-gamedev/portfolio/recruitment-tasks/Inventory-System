using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    [CreateAssetMenu(fileName = "New Spell Scroll Data", menuName = "InventoryDemo/Items/Consumable/Spell Scroll")]
    public class SpellScrollData : ConsumableItemData
    {
        [Header("Spell data")]
        [SerializeField]
        private string spellName;
        /// <summary>
        /// In a full-blown project this would be a reference to a spell data in a scriptable object/prefab
        /// </summary>
        public string SpellName { get => spellName; protected set => spellName = value; }

        public override void UseItem()
        {
            Debug.Log(string.Format("You read the scroll aloud and cast the {0} spell", SpellName));
        }
    }
}
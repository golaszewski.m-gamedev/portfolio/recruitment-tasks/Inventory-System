using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public class InventorySlot
    {
        public ItemData StoredItem { get; set; } = null;
        public uint ItemAmount { get; set; } = 0;
    }
}
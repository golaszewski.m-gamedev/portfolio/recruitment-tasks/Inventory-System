using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    public interface IInteractable
    {
        public abstract void Select();
        public abstract void Deselect();
        public abstract void Interact();
        public abstract string GetInteractionName();
    }
}
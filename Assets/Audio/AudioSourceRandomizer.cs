using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventoryDemo
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceRandomizer : MonoBehaviour
    {
        private AudioSource audioSource;

        [SerializeField]
        private float pitchOffsetRange;
        private float initialPitch;

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            initialPitch = audioSource.pitch;
            RandomizeAudioSource();
        }

        private void RandomizeAudioSource()
        {
            audioSource.pitch = initialPitch + Random.Range(-pitchOffsetRange, pitchOffsetRange);

            if (audioSource.loop)
            {
                // randomize loop starting point in case a few are playing in parallel
                audioSource.time = Random.Range(0, audioSource.clip.length);
            }
        }

        public void PlayRandomizedSound(AudioClip audio = null) 
        {
            if (audio != null)
            {
                audioSource.clip = audio;
            }

            RandomizeAudioSource();
            audioSource.Play();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace InventoryDemo
{
    public class InventoryUIController : MonoBehaviour
    {
        [Header("Item slots")]
        [SerializeField]
        private InventorySlotUIController[] slotDisplays;

        [Header("Item preview")]
        [SerializeField]
        private TextMeshProUGUI itemName;
        [SerializeField]
        private TextMeshProUGUI itemDescription;

        private void OnEnable()
        {
            RefreshInventoryDisplay();

            foreach (InventorySlotUIController slotDisplay in slotDisplays) 
            {
                slotDisplay.SlotHighlighted += DisplayItemDescroption;
            }

            InventoryManager.InventoryModified += RefreshInventoryDisplay;

            // clear the description UI
            DisplayItemDescroption(null);
        }

        private void OnDisable()
        {
            InventoryManager.InventoryModified -= RefreshInventoryDisplay;
        }

        private void RefreshInventoryDisplay()
        {
            for (int i = 0; i < InventoryManager.Inventory.Count; ++i)
            {
                slotDisplays[i].DisplaySlotContnets(InventoryManager.Inventory[i]);
            }
        }

        private void DisplayItemDescroption(ItemData item) 
        {
            if (item == null)
            {
                itemName.gameObject.SetActive(false);
                itemDescription.gameObject.SetActive(false);
            }
            else
            {
                itemName.gameObject.SetActive(true);
                itemDescription.gameObject.SetActive(true);

                itemName.text = item.ItemDisplayName;
                itemDescription.text = item.ItemDescription;
            }
        }
    }
}
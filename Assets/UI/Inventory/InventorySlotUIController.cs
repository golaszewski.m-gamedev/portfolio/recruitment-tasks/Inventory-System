using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;

namespace InventoryDemo
{
    public class InventorySlotUIController : MonoBehaviour, IPointerClickHandler, IPointerExitHandler, IPointerEnterHandler
    {
        [Header("Item representation")]
        [SerializeField]
        private GameObject itemAmountCounter;
        [SerializeField]
        private TextMeshProUGUI itemAmountNumber;

        [SerializeField]
        private Image itemImage;

        [SerializeField]
        private TextMeshProUGUI itemName;

        [SerializeField]
        private Image itemEquippedMarker;

        [Header("Item interaction")]
        [SerializeField]
        private Button useButton;
        [SerializeField]
        private TextMeshProUGUI useButtonText;
        [SerializeField]
        private Button throwAwayButton;

        private InventorySlot representedSlot;
        private string displayedItemID;

        public System.Action<ItemData> SlotHighlighted;

        private void Awake()
        {
            ToggleItemInteractionUI(false);

            useButton.onClick.AddListener(UseStoredItem);
            throwAwayButton.onClick.AddListener(DropStoredItem);
        }

        private void ToggleItemDisplayUI(bool active)
        {
            if (representedSlot != null && representedSlot.StoredItem != null
                && representedSlot.ItemAmount > 1)
            {
                itemAmountCounter.SetActive(active);
            }
            else
            {
                itemAmountCounter.SetActive(false);
            }

            if (representedSlot != null && representedSlot.StoredItem != null
                && representedSlot.StoredItem is EquippableItemData equippableItem && equippableItem.IsEquipped)
            {
                itemEquippedMarker.gameObject.SetActive(active);
            }
            else
            {
                itemEquippedMarker.gameObject.SetActive(false);
            }

            itemImage.gameObject.SetActive(active);
            itemName.gameObject.SetActive(active);
        }

        private void ToggleItemInteractionUI(bool active)
        {
            if (representedSlot != null &&
                representedSlot.StoredItem != null && representedSlot.StoredItem.Usable)
            {
                useButton.gameObject.SetActive(active);
            }
            else
            {
                useButton.gameObject.SetActive(false);
            }

            throwAwayButton.gameObject.SetActive(active);
        }

        public void DisplaySlotContnets(InventorySlot slot) 
        {
            representedSlot = slot;

            if (representedSlot.StoredItem == null)
            {
                ToggleItemDisplayUI(false);
            }
            else
            {
                ToggleItemDisplayUI(true);

                if (displayedItemID != slot.StoredItem.ItemId)
                {
                    // displayed item type changed
                    displayedItemID = slot.StoredItem.ItemId;

                    // hiding interaction buttons to prevent accidental usage 
                    // (usefull if player wanted to just use eg. multiple potions and nothing else)
                    ToggleItemInteractionUI(false);

                    // updating visuals
                    itemAmountNumber.text = representedSlot.ItemAmount.ToString();
                    itemName.text = representedSlot.StoredItem.ItemDisplayName;
                    itemImage.sprite = representedSlot.StoredItem.ItemSprite;
                    useButtonText.text = representedSlot.StoredItem.UsageName;
                }
            }
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (representedSlot != null && representedSlot.StoredItem != null)
            {
                ToggleItemInteractionUI(true);
            }
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            ToggleItemInteractionUI(false);
        }

        private void UseStoredItem() 
        {
            representedSlot.StoredItem.UseItem();
            if (representedSlot.StoredItem.Consumable) 
            {
                InventoryManager.DeleteItem(representedSlot);
            }
        }

        private void DropStoredItem()
        {
            InventoryManager.DropItem(representedSlot);
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            if (representedSlot != null)
            {
                SlotHighlighted?.Invoke(representedSlot.StoredItem);
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;

using InventoryDemo.Input;

namespace InventoryDemo.UI
{
    public class UserInterfaceController : MonoBehaviour
    {
        public static UserInterfaceController Instance;

        [Header("Gameplay")]
        [SerializeField]
        private RectTransform gameplayHUD;
        [SerializeField]
        private TextMeshProUGUI interactionText;
        [Header("Inventory")]
        [SerializeField]
        private RectTransform inventoryUI;

        private void Awake()
        {
            if (Instance != null) 
            {
                throw new System.Exception("More than one instance of " + nameof(UserInterfaceController) + " singleton detected!");
            }

            Instance = this;

            gameplayHUD.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;

            interactionText.gameObject.SetActive(false);
            inventoryUI.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            InputManager.Instance.GameplayActions.ToggleInventory.performed += EnableInventoryScreen;
            InputManager.Instance.MenuActions.ToggleInventory.performed += DisableInventoryScreen;
        }

        private void OnDisable()
        {
            InputManager.Instance.GameplayActions.ToggleInventory.performed -= EnableInventoryScreen;
            InputManager.Instance.MenuActions.ToggleInventory.performed -= DisableInventoryScreen;
        }

        public void EnableInventoryScreen(InputAction.CallbackContext context)
        {
            ToggleInventoryUI(true);
            InputManager.Instance.SwitchPlayerActionMap(InputManager.AvailableActionMaps.Menu);
            Cursor.lockState = CursorLockMode.None;
        }

        public void DisableInventoryScreen(InputAction.CallbackContext context)
        {
            ToggleInventoryUI(false);
            InputManager.Instance.SwitchPlayerActionMap(InputManager.AvailableActionMaps.Gameplay);
            Cursor.lockState = CursorLockMode.Locked;
        }

        public void ToggleInventoryUI(bool active) 
        {
            if (active)
            {
                gameplayHUD.gameObject.SetActive(false);
                inventoryUI.gameObject.SetActive(true);
            }
            else
            {
                gameplayHUD.gameObject.SetActive(true);
                inventoryUI.gameObject.SetActive(false);
            }
        }

        public void DisplayInteractionText(string text)
        {
            interactionText.gameObject.SetActive(true);
            interactionText.text = text;
        }

        public void HideInteractionText()
        {
            interactionText.gameObject.SetActive(false);
        }
    }
}